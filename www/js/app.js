var desktop = false;
var app = {
  initialize: function() { this.bindEvents(); },
  bindEvents: function() { document.addEventListener('deviceready', this.onDeviceReady, false); },
  onDeviceReady: function() { app.receivedEvent('deviceready'); },
  receivedEvent: function(id) {
    console.log('Received Event: ' + id);
    highscore.init();
    FastClick.attach(document.body);

    checkDimensions();
    $(window).resize(checkDimensions);

    if (!desktop) if (parseInt(device.version) >= 7) $('#app-wrap').addClass('ios7');
  }
};


if (!navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
  $(document).ready(function(){
  	desktop = true;
    app.receivedEvent('Dekstop ready too :)');
  });
} else {
  app.initialize();
}

/*---------------------------------------------- Get questions from server */
var questions;

function getQuestionsFromServer(){
	$.ajax({
		url: "http://www.trendquiz.instinkt.dk/content/js/questions.js",
		type: 'GET',
		crossDomain: true
	})
	.done(function() {
		console.log('Enjoy the game');
		$('#startBtn').removeClass('loading');
		$('#startBtn').addClass('ok');
	})
	.fail(function() {
		$('#startBtn').removeClass('loading');
		$('#startBtn').addClass('fail');
		console.log('FAILED');

		var msg = 'No internet connection';
		console.log( 'Error: ' + msg );
		$('.button').css('cursor', 'not-allowed');
		$('.button').html(msg);
	})
	.always(function() {
		// console.log('complete');
	});
}

getQuestionsFromServer();


/*---------------------------------------------- Check for device dimension - landscape or portrait view */
//if the width is greater than or equal to the height, show landscape view. If false show portrait  
function checkDimensions(){
	if ($(window).width() >= $(window).height()) 
		horizontal = true;
  else 
  	horizontal = false;

  if (!$('.question').hasClass('wrong') && !$('.question').hasClass('correct')) return false;

/*-------------------------------------	--------- Finder først alle classes med med selectoren answer-image - */
// Changes the width and height of the selector too 100%

  $('.answer-image').each(function(){
		if (horizontal) {
			$(this).css({width: $(this).data('height') + '%'});
			$(this).css({height: '100%'});
		} else {
			$(this).css({height: $(this).data('height') + '%'});
			$(this).css({width: '100%'});
		}
	});
}

// 500 milliseconds to next question
var timeToNextQuestion = 500;

// Start with question 1
var questionsProgress = 0;

// Number of question 
var questionAmount = 15;

// question progress
var questionsAnswered = '';

// start score is zero
var score = 0;

// contains 15 questions and images paths
var questionContainer = [];
var questionImageContainer = [];

var horizontal = false;

/*---------------------------------------------- Shows your highscore
var highscore starts with the zero from the var score. 
 */

var highscore = {
	set: function(newScore){
		highscore.score = newScore;
		localStorage.setItem('highscore', highscore.score);
		highscore.update();
	},
	init: function(){
		if (localStorage.getItem('highscore') === null) {
    	highscore.set(0);
    } else {
    	highscore.score = localStorage.getItem('highscore');
    }
    highscore.update();
	},
	update: function(){
		$('.highscore-number').html(highscore.score);
	}
};

// Shows your game score progress in the game ex. you answerd 5 out of 15 questions 
function updateProgress(){
	$('#game-score').html(questionsProgress + ' of ' + questionAmount);
}

// First it removes the class element with the class .question (selector)
// Finds the element body - then adds an class attribute called game to the body element
// Then run the nextQuestion function 
function startGame() {
	if ($('#startBtn').hasClass('loading')) {
		return false;
	} else if ($('#startBtn').hasClass('fail')) {
		getQuestionsFromServer();
	} else {
		renderAllQuestions();
		setTimeout(function(){
			$('.question').remove();
			$('body').attr('class','game');
			nextQuestion();
			loadImages();
		},100);
	}
}

// Finds the div element #game-result-score (selector), then shows the var score and the number of questions, which comes frome the var questionAmount (15)
// If your score is greater than highscore.score then set score to the number of corret answers
// Adds an class to the selector with the name result
function endGame(){
	$('#game-result-score').html(score + '<span>of</span>' + questionAmount);
	if (score > highscore.score) highscore.set(score);
	questionsProgress = 0;
	questionsAnswered = '';
	questionContainer = [];
	questionImageContainer = [];
	score = 0;
	$('body').attr('class','result');
}

// adds the class disable to the selector which makes it possible to click trough the image
//
function answer(correct, tracking, element) {
	$('.answer-image').addClass('disabled').each(function(){
		if (horizontal)
			$(this).css({width: $(this).data('height') + '%'});
		else
			$(this).css({height: $(this).data('height') + '%'});
	});

	//_gaq.push(['_trackEvent', 'Question', tracking, correct]);

	// if the function correct is true run: add the class corret to the selector.
	// else add the class wrong to the selector  
	if (correct) {
		$(element).parent().addClass('correct');
		score++;
	} else {
		$(element).parent().addClass('wrong');
	}
}

// if the progress in the game is equals to or higher than the amount of questions, run the function endGame
function nextQuestion() {

	if (questionsProgress >= questionAmount) {
		endGame();
		return false;
	}

	// nextQuestion will run renderNextQuestion
	var nextQuestions = questionContainer[questionsProgress];

	// If the progress if less than the questionAmount, add 1
	questionsProgress++;

	// next update the status of your progress
	updateProgress();

	// find the selector (id game) then add the var nextQuestion 
	$('#game').append(nextQuestions);

	// if the progress i greater than 1, then add the class left and remove the class current
	//	find the selector then remove the class right and add class current
	// do this in 1 millisecond
	if (questionsProgress > 1) {
		setTimeout(function(){
			$('.question.current').addClass('left').removeClass('current');
			$('.question.right').removeClass('right').addClass('current');
		},1);

		// remove the old question from DOM in 500 milliseconds
		setTimeout(function(){
			$('.question.left').remove();
		},500);
	}
}

function renderAllQuestions() {
	for (i = 0; i < 15; i++) {
		setTimeout(function(){
			questionContainer.push(renderNextQuestion(i));
		}, i * 1);
	}
}

function loadImages(){
	for (i = 0; i < questionImageContainer.length; i++) {
		var html = '<img class="image-loader" src="' + questionImageContainer[i] + '" style="opacity: 0;">';
		$('body').append(html);
	}
	$('.image-loader').load(function(){
		$(this).remove();
	});
}

// Find and render next random question.
function renderNextQuestion(index){

	// if it's already done dont run the function
	var done = false;
	var two;
	var one;
	var ranQuestions = shuffle(questions);

	// Find a random question for the next question
	while (!done) {
		one = questions[getRandomNumber(0,questions.length)];

		// Match finds quetions in same match category
		for (i = 0; i < ranQuestions.length; i++) {
			if (ranQuestions[i].match == one.match && ranQuestions[i] != one) {
				two = ranQuestions[i];
			}
		}

		var combo1 = one.image+two.image;
		var combo2 = two.image+one.image;

		if (questionsAnswered.indexOf(combo1) == -1 && questionsAnswered.indexOf(combo2) == -1){
			done = true;
			questionsAnswered = questionsAnswered + one.image + two.image + two.image + one.image;
		}
	}

	var ratio = one.rank > two.rank? one.rank / two.rank : two.rank / one.rank;
	var correct = one.rank > two.rank? one.name : two.name;
	var incorrect = one.rank > two.rank? two.name : one.name;

	one.correct = one.rank > two.rank? true : false;
	two.correct = one.rank < two.rank? true : false;
	one.lessMore = one.rank > two.rank? 'more' : 'less';
	two.lessMore = one.rank < two.rank? 'more' : 'less';
	one.newHeight = 100 / (one.rank + two.rank) * one.rank;
	two.newHeight = 100 / (one.rank + two.rank) * two.rank;

	firstTime = questionContainer.length <= 0? 'current' : 'right';

	if (ratio > 5)
		ratio = ratio.toFixed();
	else if (ratio < 1.1)
		ratio = ratio.toFixed(2);
	else
		ratio = ratio.toFixed(1);

	// share your result to desired social media if Simon decided this is awesome.
	// var share = desktop? '' : '<div class="button" onclick="shareQuestion({width: ' + one.newHeight.toFixed(1) + ', img: \'img/questions/' + one.image + '.jpg\'},{width: ' + two.newHeight.toFixed(1) + ', img: \'img/questions/' + two.image + '.jpg\'},[\'' + correct + '\', \'' + incorrect + '\', ' + ratio + '])">Share...</div>'
	var share = '';
	
	questionImageContainer.push('http://trendquiz.instinkt.dk/content/images/' + one.image + '.jpg');
	questionImageContainer.push('http://trendquiz.instinkt.dk/content/images/' + two.image + '.jpg');

	var question = 
	'<div class="question ' + firstTime + '"> \
		<div class="answer-image" data-height="' + one.newHeight.toFixed(1) + '" onclick="answer(' + one.correct + ', \'' + (one.name + ' vs. ' + two.name)  + '\', this)" style="background-image: url(http://trendquiz.instinkt.dk/content/images/' + one.image + '.jpg)"> \
			<h2 class="answer-text">' + one.name + '</h2> \
		</div> \
		<div class="answer-image" data-height="' + two.newHeight.toFixed(1) + '" onclick="answer(' + two.correct + ', \'' + (one.name + ' vs. ' + two.name)  + '\', this)" style="background-image: url(http://trendquiz.instinkt.dk/content/images/' + two.image + '.jpg)"> \
			<h2 class="answer-text">' + two.name + '</h2> \
		</div> \
		<div class="question-result correct"> \
			<div class="page-content"> \
				<h1>Correct!</h1> \
				<h3>"' + correct + '" was googled<br> ' + ratio + ' times as much as "' + incorrect + '"</h3> \
			<div class="button" onclick="nextQuestion()">Next</div>' 
			+ share + 
		'</div> \
		</div> \
		<div class="question-result wrong"> \
			<div class="page-content"> \
				<h1>Wrong!</h1> \
				<h3>"' + correct + '"" was googled<br> ' + ratio + ' times as much as "' + incorrect + '"</h3> \
				<div class="button" onclick="nextQuestion()">Next</div>' 
				+ share + 
			'</div> \
		</div> \
	</div>';

	 return question;
}

function getRandomNumber(from, to) {
	return Math.floor(Math.random() * (to - from) + from);
}

function shuffle(o){
  for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
  return o;
};

function shareQuestion(one, two, info){

	var text = 'In 2013 "' + info[0] + '" was googled ' + info[2] + ' times as much as "' + info[1] + '". #TrendQuiz for iOS and Android!';

	if (!desktop) return false;
	//window.plugins.socialsharing.share(text, null, null, null);

	getSocialImage(one,two);
	/*$.when(getSocialImage(one, two)).done(function(data){
		window.plugins.socialsharing.share(text, null, 'data:image/png;base64,R0lGODlhDAAMALMBAP8AAP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAUKAAEALAAAAAAMAAwAQAQZMMhJK7iY4p3nlZ8XgmNlnibXdVqolmhcRQA7', null);
	});*/
}

function getSocialImage(one, two) {

	var dfd1 = $.Deferred();

	function drawCanvas(){
		
		$('body').append('<canvas id="socialImage" style="display: none;" crossorigin="anonymous" width="500" height="360"></canvas>');

    var dfd2 = $.Deferred(),
    		canvas = document.getElementById('socialImage'),
    		context = canvas.getContext('2d'),
    		imageObj = [new Image(), new Image()];

    var sourceX = [ (one.width * -1 + 100) * 10 / 2, (two.width * -1 + 100) * 10 / 2 ],
    		sourceY = 175,
    		sourceWidth = [ 1000 / (100 / one.width), 1000 / (100 / two.width) ],
    		sourceHeight = 1000,
    		destWidth = [ (1000 / 2) / (100 / one.width), (1000 / 2) / (100 / two.width) ],
    		destHeight = sourceHeight / 2,
    		destX = [0,one.width * 5],
    		destY = 0;

    imageObj[0].src = one.img;
    imageObj[1].src = two.img;

    context.drawImage(imageObj[0], sourceX[0], sourceY, sourceWidth[0], sourceHeight, destX[0], destY, destWidth[0], destHeight);
    context.drawImage(imageObj[1], sourceX[1], sourceY, sourceWidth[1], sourceHeight, destX[1], destY, destWidth[1], destHeight);
    dfd2.resolve();

    return dfd2;
  }

  $.when(drawCanvas()).done(function(){
  	oCanvas = document.getElementById("socialImage");  
  	dfd1.resolve(oCanvas.toDataURL());
  	window.location = oCanvas.toDataURL();0
  	oCanvas.remove();
  });

  return dfd1;
}

